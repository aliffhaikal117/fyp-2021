## Basic raspberry pi and python function

1. _To change directory_

``` js
cd

```

2. _To check version_

``` js
python/python3/python3.x.x

```

3. _To download package information from all configured sources_

``` js
sudo apt-get update

```


4. _Downloads and installs the updates for each outdated package and dependency on your system_

``` js
sudo apt-get upgrade

```

_sudo enable administrator permission_


## How to change Python version in Raspberry pi

1. Open Terminal

2. Check Version 

``` js
python --version

``` 
3. Download wanted Python version from website [HERE!!](https://www.python.org/downloads/)

4.Extract the file 

``` js
tar xf Python-3.9.0.tar.xz

``` 
5. Change directories

``` js
cd Python-3.x.x

``` 
6. Just follow
``` js
./configure --enable-optimizations
make
sudo make install

``` 

## How to download yolov5 requirement into raspberry pi

To download yolov5 into raspberry pi, we need to install all of the libraries one by one to avoid installing wrong version of the libraries. By checking the requirements.txt that was provided in yolov5 folder, we can install it according to the .txt file. 

``` js
pip3 install -libraries wanted-

``` 

However, torch and torchvision need to be install by using another method. These libraries need to be search in the github and then find the link to be install using command below

``` js
pip3 install -github link-
```




