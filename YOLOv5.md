# YOLOv5 

![](https://github.com/ultralytics/yolov5/releases/download/v1.0/splash.jpg)

## Introduction to YOLOv5

YOLO an acronym for 'You only look once', is an object detection algorithm that divides images into a grid system. Each 
cell in the grid is responsible for detecting objects within itself.YOLO is one of the most famous object detection algorithms due to its speed and accuracy.

Shortly after the release of YOLOv4 Glenn Jocher introduced YOLOv5 using the Pytorch framework.

The open source code is available on GitHub

Author: Glenn Jocher
Released: 18 May 2020

## Step-by step guide to do object detection using YOLOv5

1. Find dataset(same amount of images per classes)

2. Register roboflow and Google Collab

### Roboflow (Data Processing Task)

1. Upload dataset

2. Annotate dataset

3. Preprocessing  

4. Export processed dataset into code

### Google Collab (Training Model Task)
 
1. Install YOLOv5 requirement

```js
git clone https://github.com/ultralytics/yolov5  # clone
cd yolov5
pip install -r requirements.txt  # install

```
2. Import dataset from Roboflow

```js
!pip install roboflow

from roboflow import Roboflow
rf = Roboflow(api_key="xxxxxxxxxxxxxxxx")
project = rf.workspace("xxxxxxxxx").project("xxxxxxxxxxxxxxxxxxxx")
dataset = project.version(xxxx).download("yolov5")

```
3. Training with dataset

```js
!python train.py --data xxxx.yaml --cfg xxxxx.yaml --weights '' --epoch 100 --batch-size 32 

```
4. Download trained model into local

```js
!zip -r /content/yolov5.zip /content/yolov5
from google.colab import files
files.download("/content/yolov5.zip")

```

5. Inference with image

```js
!python detect.py --source xxxx.jpg  --weights best.pt

```

6. Inference with video

```js
!python detect.py --source xxxx.mp4  --weights best.pt

```

#Train custom data using robolytics dataset

## Result

![](https://gitlab.com/aliffhaikal117/fyp-2021/-/raw/main/Images/results.png)

### Precision and Recall
### Loss function

1.Classification Loss

2.Objectness Loss

3.Box Loss
