<div align="middle">

# Final Year Project

## Title: Moving Object Detection and Identification for UAV

[Update on Progress](#update)<br>
[References](#references-for-fyp)<br>
[__Video about overall project__](https://www.youtube.com/watch?v=QnCbgDeAEjg)

</div>

### Goals
|(/)|Goals|
|---|---|
|/|Understand what is image detection|
|/|Create an algorithm for image detection using image|
|/|Create an algorithm for image detection using video|
|/|Learn about opencv|
|/|Understand about models of object detection|
|/|Create an algorithm for image detection using video to detect multiple object|
|/|Evaluate model|

### 2022
|(/)|Goals|
|---|---|
|/|Try annotate|
|/|Search for images|
|/|Try to code in google collab|
|/|Train the model|
|/|Searching for proper camera|
|/|Search for proper equipment to save data from camera|
|/|Usage of deep learning(YOLO/SSD)|
||Integration with RPI|
||Report on FYP|
||Video presentation|

Guide
_https://www.youtube.com/watch?v=HYWS4jh0i4Y_
### Problem and Solution
1. The output when running any coding turns out to be error
``` js
frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
cv2.error: OpenCV(4.4.0) C:\Users\appveyor\AppData\Local\Temp\1\pip-req-build-nxx381if\opencv\modules\imgproc\src\color.cpp:182: error: (-215:Assertion failed) !_src.empty() in function 'cv::cvtColor'

``` 


### 1/11/2021

__Open CV__

Learn about read image using open cv

[<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ_MXoDzILGVFYIwFzFvPt8TqxTMIE8RM61LQ&usqp=CAU" width=300 align=middle>](https://www.youtube.com/watch?v=qCR2Weh64h4)


``` js

import cv2

img=cv2.imread('image location.png')      #choose image
#img=cv2.resize(img,(400,400))            #image resizing according to pixel
img=cv2.resize(img,(0,0),fx=3.5,fy= 3.5)  #image resizing according to percentage of image size
cv2.imshow('Image',img)                   #show image
cv2.waitKey(0)                            #Time taken to close image, 0 for infinite
cv2.destroyAllWindows()                   #Close the image

```

### 3/11/2021

<div align="middle">

__Flowchart__


<img src="https://gitlab.com/aliffhaikal117/fyp-2021/-/raw/main/Images/Untitled%20Diagram.drawio.png" width=500 align=middle>

</div>

### Update

### 9/11/2021

_Object Recognition Problems_

1. Lighting
2. Positioning of the object
3. Rotation of the object
4. Mirroring
5. Occlusion-object hides behind other object
6. Scale-change of size from original object size

Technique

1. Template Matching
 - pixel-by-pixel matching

2. Blob analysis


**Pretrained Object Detection Algorithm**

1. Region Network Convulutional Network (RCNN)
<img src="https://gitlab.com/aliffhaikal117/fyp-2021/-/raw/main/Images/download.png" width=300 align=middle>



2. Fast RCNN
3. YOLO
<img src="https://www.researchgate.net/publication/342090227/figure/fig1/AS:901048621334565@1591838140526/The-detection-model-of-YOLO.jpg" width=300 align=middle>



4. SSD
5. Mask RCNN
<img src="https://production-media.paperswithcode.com/methods/Screen_Shot_2020-05-23_at_7.44.34_PM.png" width=300 align=middle>


6. Multibox
7. Resnet
8. GoogLeNet

__Create line/rectangle/text using opencv__

``` js

import cv2

img=cv2.imread('image location.png')                                   #choose image
cv2.line(img,(0,0),(150,150),(255,255,255),1)                          #create straight line [image,(start line),(end line),(colour),thickness]
cv2.rectangle(img,(1,2),(160,150),(0,0,255),1)                         #create rectangle [image,(start line),(end line),(colour),thickness]

font= cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img,'Study rajin2',(0,130),font,1,(255,0,0),2,cv2.LINE_AA) #create text[image,'Text',(start position),font type,text size,(colour),text thickness,anti-aliased line]
cv2.imshow('Image',img)                                                #show image
cv2.waitKey(0)                                                         #Time taken to close image, 0 for infinite
cv2.destroyAllWindows()                                                #Close the image

```
More information on colour click [HERE!!](https://stackoverflow.com/questions/48294965/what-is-the-value-of-the-bgr-in-opencv)


__Dr comment__

- Tengok references dari sebelum2 ni punya journal 
- Final product-boleh detect kucing-add on jenis 
- Cuba explain coding orang tu buat apa
- Boleh je buat coding macam face detection gitu.

[Futher read for RCNN geng and YOLO models](https://www.analyticsvidhya.com/blog/2018/10/a-step-by-step-introduction-to-the-basic-object-detection-algorithms-part-1/)
### Training the model


[SSD Example Code](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Example%20Algorithm/SSD_MOD.py)

Explanation
1. Line 1-5 – Importing libraries required for Object Detection using SSD.
2. Line 7-15 – Defining some constants and Classes array. Our SSD model is trained on these 21 classes.
3. Line 17 – Defining colors array where each class is randomly assigned a color.
4. Line 19 – Reading the network in a variable called net using cv2.dnn.readNetFromCaffe.
5. Line 21-24 – If the parameter use_gpu is set to TRUE, set the backend and target to Cuda.
6. Line 27-31 – Initialize the VideoCapture object either with 0 for live video or with the video file name.
7. Line 34 – Let’s get in the infinite array and read the frames.
8. Line 35 – If ret says that if the VideoCapture object is returning True, then only proceed.
9. Line 36-37 – Resize the frame and get its height and width.
10. Line 39-41 – Create a blob from the image, set it as input, and pass it forward through the network using cv2.blobFromImage.
11. Line 43 – Let’s traverse in the detections we got.
12. Line 44 – Let’s check the confidence of each and every detection.
13. Line 45 – If the confidence is greater than a threshold then proceed.
14. Line 47-48 – Calculate the coordinates of the box and convert them to int.
15. Line 50 – Creating text like ‘Class_name: confidence%’.
16. Line 51 – Drawing the rectangle around the found object.
17. Line 53-54 – Finally putting this label onto the original frame.
18. Line 56 – Show final results.
19. Line 58-59 – Break if someone hits the ESC key.
20. Line 61 – Update the fps counter.
21. Line 63 – Stop the fps counter.
22. Line 65-66 – Printing FPS metrics.

### Read,Write and Display Video

_Read_
``` js
# Create a VideoCapture object and read from input file
# If the input is taken from the camera, pass 0 instead of the video file name.
cap = cv2.VideoCapture('chaplin.mp4')
``` 
_Display_
``` js
import cv2
import numpy as np
 
# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('chaplin.mp4')
 
# Check if camera opened successfully
if (cap.isOpened()== False):
  print("Error opening video stream or file")
 
# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
 
    # Display the resulting frame
    cv2.imshow('Frame',frame)
 
    # Press Q on keyboard to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else:
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()
``` 
_Write_
``` js
# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
# Define the fps to be equal to 10. Also frame size is passed.
out = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame_width,frame_height))
``` 
_Full_
``` js
import cv2
import numpy as np
 
# Create a VideoCapture object
cap = cv2.VideoCapture(0)
 
# Check if camera opened successfully
if (cap.isOpened() == False):
  print("Unable to read camera feed")
 
# Default resolutions of the frame are obtained.The default resolutions are system dependent.
# We convert the resolutions from float to integer.
frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
 
# Define the codec and create VideoWriter object.The output is stored in 'outpy.avi' file.
out = cv2.VideoWriter('outpy.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame_width,frame_height))
 
while(True):
  ret, frame = cap.read()
 
  if ret == True:
     
    # Write the frame into the file 'output.avi'
    out.write(frame)
 
    # Display the resulting frame   
    cv2.imshow('frame',frame)
 
    # Press Q on keyboard to stop recording
    if cv2.waitKey(1) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else:
    break 
 
# When everything done, release the video capture and video write objects
cap.release()
out.release()
 
# Closes all the frames
cv2.destroyAllWindows()
``` 


### References for FYP

|No.|Title|Author|
|-----|----|----|
|1|[Object Detection Using Image Processing](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/1.pdf)|_F. Jalled and I. Voronkov_|
|2|[Detection and Tracking of Moving UAVs](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/2.pdf)|*J. Janousek, P. Marcon, J. Pokorny, and J. Mikulka*|
|3|[Trajectory and image-based detection and identification of UAV](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/3.pdf)|*Y. Liu et al.*|
|4|[Object detection for drones on Raspberry Pi potentials and challenges](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/4.pdf)|*T. Q. Khoi, N. A. Quang, and N. K. Hieu*|
|5|[Object Detection and Tracking with UAV Data Using Deep Learning](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/5.pdf)|*A. A. Micheal, K. Vani, S. Sanjeevi, and C.-H. Lin*|
|6|[A Guide for Machine Vision in Quality Control](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/A_Guide_for_Machine_Vision_in_Quality_Control_by_Sheila_Anand_L._Priya__z-lib.org_.pdf)|*S. Anand and L. Priya*|
|7|[Computer Vision and Applications](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/7.pdf)|*B. Jähne and H. Haussecker*|
|8|[Hands-On Deep Learning for Images with Tensorflow](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/8.pdf)|W. Ballard|
|10|[Learn Computer Vision using OpenCV](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/10.pdf)|[1]S. Gollapudi|
|11|[Machine Learning for OpenCV4](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/11.pdf)|*[1]A. Sharma, V. R. Shrimali, and M. Beyeler*|
|12|[Python Machine Learning Projects](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/12.pdf)|*-*|
|13|[Moving object detection and tracking Using Convolutional Neural Networks](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/13.pdf)|*S. Mane and S. Mangale*|
|14|[Objects Talk - Object detection and Pattern Tracking using TensorFlow](https://gitlab.com/aliffhaikal117/fyp-2021/-/blob/main/Pdf/14.pdf)|*R. Phadnis, J. Mishra, and S. Bendale*|
|15|[]()|**|
|16|[]()|**|
|17|[]()|**|



